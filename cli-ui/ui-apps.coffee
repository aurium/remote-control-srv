apps.onOpen = ->
  if $('#appList li').length is 0
    $('<span class="placeholder">Search for apps to run</span>').appendTo appList
    timeout 1100, -> do appSearch.focus

RC.socket.on 'updateApps', (apps)->
  RC.apps = apps

$(appSearch).on 'change', ->
  do appSearch.blur
  findApps appSearch.value unless /^\s*$/.test appSearch.value

findApps = (query)->
  query = query.replace(/^\s*|\s*$/, '').replace(/\s+/, ' ').toLowerCase()
  do $(appList).empty
  for codeName,app of RC.apps
    appStr = [ app.name, app.description, app.categories.join ' ' ].join ' '
    appStr = do appStr.toLowerCase
    console.log appStr
    if appStr.indexOf(query) > -1
      console.log 'find', app
      $("<li><i style=\"background-image:url(/app-icon/#{app.icon})\"></i>
        #{app.name}</li>").appendTo(appList).data(codeName: app.codeName).click ->
          el = $ this
          RC.socket.emit 'run-app', el.data 'codeName'
          el.addClass 'touched'
          timeout 1000, -> el.removeClass 'touched'

