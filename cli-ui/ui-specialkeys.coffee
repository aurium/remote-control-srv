specialKeysList = {
  'F1' :    { code: 'F1'  }
  'F2' :    { code: 'F2'  }
  'F3' :    { code: 'F3'  }
  'F4' :    { code: 'F4'  }
  'F5' :    { code: 'F5'  }
  'F6' :    { code: 'F6'  }
  'F7' :    { code: 'F7'  }
  'F8' :    { code: 'F8'  }
  'F9' :    { code: 'F9'  }
  'F10':    { code: 'F10' }
  'F11':    { code: 'F11' }
  'F12':    { code: 'F12' }
  '\u2192Del': { code: 'Delete'    }
  '\u2190Del': { code: 'BackSpace' }
  'Esc'      : { code: 'Escape'    }
  'Tab'      : { code: 'Tab'       }
  'Ctrl'     : { code: 'Control_L' }
  'Alt'      : { code: 'Alt_L'     }
  'AltGr'    : { code: 'ISO_Level3_Shift' }
  'PgUp'     : { code: 'Page_Up'   }
  'PgDn'     : { code: 'Page_Down' }
  'Home'     : { code: 'Home'      }
  'End'      : { code: 'End'       }
  'Ins'      : { code: 'Insert'    }
  'Super'    : { code: 'Super_L'   }
  '^C'       : { code: '^C'        }
  '\u2190'   : { code: 'Left'      }
  '\u2192'   : { code: 'Right'     }
  '\u2191'   : { code: 'Up'        }
  '\u2193'   : { code: 'Down'      }
}

for key,data of specialKeysList
  cssClass = "name-len-#{key.length} name-#{key}"
  $("<li class=\"#{cssClass}\">#{key}</li>").on('click', ->
    RC.socket.emit 'key-press', $(@).data('key')
  ).data(key: data.code).appendTo theKeys

