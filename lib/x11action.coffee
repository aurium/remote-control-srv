xClient = xTest = xKeymap = xRoot = null

exports.init = (callback)->
  keymap = require('./x11keymap').init (err, keymap, x11, display)->
    return callback err if err
    xClient = display.client
    xKeymap = keymap

    xRoot = display.screen[0].root
    display.client.require 'xtest', (err, test)->
      xTest = test
      throw err if err
      callback null, exports, x11, display

exports.keyPress = keyPress = (key)->
  for k in xKeymap.splitComplexChar key
    k = xKeymap.findKey k
    if k?
      for key in k.composition
        xTest.FakeInput xTest.KeyPress, key.code, 0, xRoot, 0, 0
      for key in k.composition by -1
        xTest.FakeInput xTest.KeyRelease, key.code, 0, xRoot, 0, 0
    else
      console.error "Cant find char \"#{key}\"."

exports.getScreenDimnsion = (callback)->
  xClient.GetGeometry xRoot, callback

exports.mouseMove = (x, y)->
  xTest.FakeInput xTest.MotionNotify, false, 0, xRoot, x, y

exports.mouseClick = (bt)->
  bt = if bt is 'left' then 1 else 3
  console.log 'Mouse click', bt
  xTest.FakeInput xTest.ButtonPress, bt
  xTest.FakeInput xTest.ButtonRelease, bt

exports.inputText = (txt)->
  @keyPress char for char in txt

