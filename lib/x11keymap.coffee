#run = require('child_process').spawnSync
#rawTable = run('xmodmap', ['-pke']).stdout.toString().split '\n'

MODULE_NAME = __filename.replace /^.*\/|\.[^.]+$/g, ''
console.log 'load', MODULE_NAME

x11 = require 'x11'
exports.table = table = []
exports.modifierKey = modifierKey = {}
keySyms = null
ks2Name = []
ks2Desc = []

exports.init = (callback)->
  console.log "#{MODULE_NAME} Starting..." if process.env.DEBUG
  x11.createClient (err, display)->
    if err
      if callback then return callback err else throw err
    for codeName,keyData of keySyms=x11.keySyms
      ks2Name[keyData.code] = codeName
      ks2Desc[keyData.code] = keyData.description
    [min, max] = [display.min_keycode, display.max_keycode]
    display.client.GetKeyboardMapping min, max-min, (err, list)->
      if err
        if callback then return callback err else throw err
      for sublist,i in list
        table[i+min] = for codeNum in sublist[0..5]
          if ks2Name[codeNum]?
            codeName = ks2Name[codeNum].replace /^XK_/g, ''
            #if codeName.length is 1 then codeName else codeName.toLowerCase()
            codeName = codeName.toLowerCase() if codeName.length > 1
            { name: codeName, desc: ks2Desc[codeNum] }
          else
            codeNum
      modifierKey.modeSwitch = { code: 0xFF7E, description: 'Switch' }
      modifierKey.altGr = { code: findKey('iso_level3_shift').keycode, description: 'AltGr' }
      modifierKey.shift = { code: findKey('shift_l').keycode, description: 'Shift' }
      modifierKey.ctrl  = { code: findKey('control_l').keycode, description: 'Control' }
      keyCache['^c'] = new Key '^c', findKey('c').keycode, 'ctrl+Key'
      console.log "#{MODULE_NAME} Started." if process.env.DEBUG
      if callback
        callback null, module.exports, x11, display # Async mode
  module.exports # Sync mode

keyComposition = [
  'Key'
  'shift+Key'
  'modeSwitch+Key'
  'modeSwitch+shift+Key'
  'altGr+Key'
  'altGr+shift+Key'
]

class Key
  constructor: (@name, @keycode, @rawComposition) ->

Object.defineProperty Key::, 'composition', get: ->
  if process.env.DEBUG
    console.log "#{MODULE_NAME} build keycode composition for #{@name}."
  keyStates = table[@keycode]
  list = for k,index in @rawComposition.split '+'
    if k is 'Key'
      { code: @keycode, description: keyStates[index].desc }
    else
      { code: modifierKey[k].code, description: modifierKey[k].description }
  # Cache it:
  Object.defineProperty this, 'composition', value: list
  list

keyCache = {}
exports.findKey = findKey = (name)->
  name = translateCharToName name
  name = name.toLowerCase() if name.length > 1
  if keyCache[name]
    return keyCache[name]
  else
    for keyStates,keycode in table
      if keyStates?
        index = keyStates.map((k)-> k.name).indexOf name
        if index > -1
          return keyCache[name] = new Key name, keycode, keyComposition[index]
  null

exports.translateCharToName = translateCharToName = (char)->
  {
    '\n': 'return'
    '?': 'question'
    '!': 'exclam'
    '.': 'period'
    ',': 'comma'
    ';': 'semicolon'
    ':': 'colon'
    '|': 'bar'
    '/': 'slash'
    '\\': 'backslash'
    '^': 'dead_circumflex'
    '~': 'dead_tilde'
    '¨': 'dead_diaeresis'
    '˝': 'dead_doubleacute'
    '¯': 'dead_macron'
    '´': 'dead_acute'
    '`': 'dead_grave'
    '{': 'braceleft'
    '}': 'braceright'
    '[': 'bracketleft'
    ']': 'bracketright'
    '(': 'parenleft'
    ')': 'parenright'
    '$': 'dollar'
    '¢': 'cent'
    '£': 'sterling'
    '€': 'EuroSign'
    '@': 'at'
    '#': 'numbersign'
    '%': 'percent'
    '&': 'ampersand'
    '*': 'asterisk'
    '"': 'quotedbl'
    "'": 'apostrophe'
    '_': 'underscore'
    '-': 'minus'
    '+': 'plus'
    '=': 'equal'
    '§': 'section'
    '¹': 'onesuperior'
    '²': 'twosuperior'
    '³': 'threesuperior'
    '¬': 'notsign'
    'ç': 'ccedilla'
    'Ç': 'Ccedilla'
    '©': 'copyright'
    'ª': 'ordfeminine'
    'º': 'masculine'
    ' ': 'space'
  }[char] || char

exports.splitComplexChar = (char)->
  {
    '^': ['dead_circumflex', ' ']
    '~': ['dead_tilde', ' ']
    '¨': ['dead_diaeresis', ' ']
    '˝': ['dead_doubleacute', ' ']
    '¯': ['dead_macron', ' ']
    '´': ['dead_acute', ' ']
    '`': ['dead_grave', ' ']
    'â': ['dead_circumflex', 'a']
    'ã': ['dead_tilde', 'a']
    'ä': ['dead_diaeresis', 'a']
    'á': ['dead_acute', 'a']
    'à': ['dead_grave', 'a']
    'ê': ['dead_circumflex', 'e']
    'ẽ': ['dead_tilde', 'e']
    'ë': ['dead_diaeresis', 'e']
    'é': ['dead_acute', 'e']
    'è': ['dead_grave', 'e']
    'î': ['dead_circumflex', 'i']
    'ĩ': ['dead_tilde', 'i']
    'ï': ['dead_diaeresis', 'i']
    'í': ['dead_acute', 'i']
    'ì': ['dead_grave', 'i']
    'ô': ['dead_circumflex', 'o']
    'õ': ['dead_tilde', 'o']
    'ö': ['dead_diaeresis', 'o']
    'ó': ['dead_acute', 'o']
    'ò': ['dead_grave', 'o']
    'û': ['dead_circumflex', 'u']
    'ũ': ['dead_tilde', 'u']
    'ü': ['dead_diaeresis', 'u']
    'ú': ['dead_acute', 'u']
    'ù': ['dead_grave', 'u']
    'Â': ['dead_circumflex', 'A']
    'Ã': ['dead_tilde', 'A']
    'Ä': ['dead_diaeresis', 'A']
    'Á': ['dead_acute', 'A']
    'À': ['dead_grave', 'A']
    'Ê': ['dead_circumflex', 'E']
    'Ẽ': ['dead_tilde', 'E']
    'Ë': ['dead_diaeresis', 'E']
    'É': ['dead_acute', 'E']
    'È': ['dead_grave', 'E']
    'Î': ['dead_circumflex', 'I']
    'Ĩ': ['dead_tilde', 'I']
    'Ï': ['dead_diaeresis', 'I']
    'Í': ['dead_acute', 'I']
    'Ì': ['dead_grave', 'I']
    'Ô': ['dead_circumflex', 'O']
    'Õ': ['dead_tilde', 'O']
    'Ö': ['dead_diaeresis', 'O']
    'Ó': ['dead_acute', 'O']
    'Ò': ['dead_grave', 'O']
    'Û': ['dead_circumflex', 'U']
    'Ũ': ['dead_tilde', 'U']
    'Ü': ['dead_diaeresis', 'U']
    'Ú': ['dead_acute', 'U']
    'Ù': ['dead_grave', 'U']
  }[char] || [char]

