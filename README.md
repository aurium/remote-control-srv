Remote Control
==============

Use your smartphone as mouse, keyboard or gamepad for your desktop.

## How to install

Install this application on your desktop. You can to by cloning:
```bash
$ git clone https://gitlab.com/aurium/remote-control-srv.git
$ cd remote-control-srv
$ npm install
```

## How to use

Firstly, start the server:
```bash
# you should be inside remote-control-srv directory
$ npm start
```

That will write an output like this:
`Server is On at ABC http://192.168.X.Y:2345.`

Open the printed address on your smartphone's web browser.

You will see a big dark gray rectangle. This is your mouse pad.

The left buttons are you contextual menu.

Enjoy.
