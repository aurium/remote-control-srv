# Ctrl button actions
$('.btMouseRight').on 'click', -> RC.socket.emit 'mouse-click', 'right'

# Tap recognize:
tapIni = false

mousePad.addEventListener 'touchstart', (ev)->
  do RC.fullsreen
  RC.androidTouchmoveWorkaround ev
  tapIni = true

mousePad.addEventListener 'touchmove', (ev)->
  RC.androidTouchmoveWorkaround ev
  touches = RC.getNativeEvent(ev).touches;
  if touches.length is 1
    t = touches[0]
    x = (t.clientX - mousePad.offsetLeft) / mousePad.clientWidth
    y = (t.clientY - mousePad.offsetTop) / mousePad.clientHeight
    x = 0 if x < 0
    x = 1 if x > 1
    y = 0 if y < 0
    y = 1 if y > 1
    RC.socket.emit 'mouse-move', x:x, y:y
  tapIni = false

mousePad.addEventListener 'touchcancel', (ev)->
  RC.socket.emit 'debug', 'touch-cancel'
  tapIni = false

mousePad.addEventListener 'touchend', (ev)->
  RC.socket.emit 'mouse-click', 'left' if tapIni
  tapIni = false
