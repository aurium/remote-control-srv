realLastTab = null

keyboard.onOpen = (lastTab)->
  console.log '=>', lastTab
  realLastTab = lastTab if lastTab isnt specialKeys
  textInput.value = ''
  timeout 500, -> do textInput.focus

$('.btCloseKeyboard').on 'click', ->
  console.log '>>>>', realLastTab
  RC.showUItab realLastTab

$('.btSendText').on 'click', ->
  RC.socket.emit 'input-text', textInput.value
  textInput.value = ''
  textInput.focus()

$('.btKeyLeft').on 'click', ->
  RC.socket.emit 'key-press', 'Left'
  textInput.focus()

$('.btKeyRight').on 'click', ->
  RC.socket.emit 'key-press', 'Right'
  textInput.focus()

$('.btKeyDel').on 'click', ->
  RC.socket.emit 'key-press', 'Delete'
  textInput.focus()

$('.btSpecialKeys').on 'click', -> RC.showUItab specialKeys


textInput.onchange = -> btSendText.onclick

