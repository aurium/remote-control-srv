window.RC = {} # Remote Control global namespace
console.error = console.log unless console.error

window.timeout = (delay, callback)-> setTimeout callback, delay

RC.socket = do io

docEl = document.documentElement
requestFullScreen = docEl.requestFullscreen       or \
                    docEl.mozRequestFullScreen    or \
                    docEl.webkitRequestFullScreen or \
                    docEl.msRequestFullscreen
RC.fullsreen = -> requestFullScreen.call docEl

$(document.body).click -> do RC.fullsreen

# Lock rotation
try
  screen.lockOrientationUniversal = (
    if screen.orientation
      screen.orientation.lock
    else
      screen.lockOrientation    or \
      screen.mozLockOrientation or \
      screen.msLockOrientation
  ) or (->)
  screen.lockOrientationUniversal 'landscape'
catch err
  RC.socket.emit 'debug', fail: err.message
  #console.error 'lockOrientationUniversal Fail', screen.lockOrientationUniversal, err

# http://stackoverflow.com/questions/11204460/the-touchmove-event-on-android-system-transformer-prime
RC.androidTouchmoveWorkaround = (ev)->
  do ev.preventDefault if navigator.userAgent.match /Android/i

# From jQuery.Mobile
RC.getNativeEvent = (event)->
  event = event.originalEvent while event?.originalEvent?
  event

uiTabs = $('.UI-tab')
RC.showUItab = (showTab)->
  console.log 'Show UI', showTab.id, showTab
  lastTab = $('.UI-tab.visible')[0]
  uiTabs.removeClass('visible').addClass 'hidden'
  $(showTab).removeClass('hidden').addClass 'visible'
  showTab.onOpen lastTab if showTab.onOpen

# Shared ctrl buttons
$('.btApps').on 'click', -> RC.showUItab apps
$('.btMouse').on 'click', -> RC.showUItab mouse
$('.btKeyboard').on 'click', -> RC.showUItab keyboard
$('.btGamepad').on 'click', -> RC.showUItab gamepad
$('.btConfig').on 'click', -> RC.showUItab config

