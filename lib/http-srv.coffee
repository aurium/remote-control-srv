round = Math.round
fs = require 'fs'
http = require 'http'
apps = require './apps'
{getOutput} = require './helpers'
{spawn} = require 'child_process'

do apps.loadAppList

cliDir = "#{__dirname}/../cli-ui"

loadFileContent = (file)->
  fs.readFileSync("#{cliDir}/#{file}").toString()

html = loadFileContent 'index.html'
scss = "#{cliDir}/style.scss"
cliCSS = require('node-sass').renderSync(file: scss).css.toString()

srv = http.createServer (req, res)->

  if req.url is '/'
    res.writeHead 200, 'Content-Type': 'text/html'
    res.end html.replace(/#CSS#/, cliCSS)

  else if req.url is '/apk'
    console.log 'Ship APK!'
    res.end fs.readFileSync __dirname + '/../remote-control.apk'

  else if /^\/public\//.test req.url
    path = req.url.replace /\/\.+\//, '/'
    console.log 'Request file:', path
    try
      mime = switch path.replace /^.*\./g, '' # get from file extension
        when 'svg' then 'image/svg+xml'
        else 'text/plain'
      res.writeHead 200, 'Content-Type': mime
      res.end loadFileContent path
    catch err
      res.writeHead 404, 'Content-Type': 'text/plain'
      res.end "Cant find #{path}"

  else if /^\/app-icon\//.test req.url
    icon = req.url[10..]
    path = apps.findIcon icon
    fs.readFile path, (err, data)->
      if err
        console.log 'Request Icon FAIL for', icon, path
        res.writeHead 404, 'Content-Type': 'text/plain'
        res.end "404 - Can't find #{icon}.\n#{path}"
      else
        mime = switch path.replace /^.*\./g, '' # get from file extension
          when 'svg' then 'image/svg+xml'
          when 'png' then 'image/png'
          when 'jpg' then 'image/jpeg'
          else 'text/plain'
        res.writeHead 200, 'Content-Type': mime
        res.end data

  else if /^\/js\//.test req.url
    script = req.url.replace /^\/js\/|\/\.+\//g, '/'
    console.log 'Request JS:', script
    fs.stat "#{cliDir}/#{script}", (err, stats)->
      if err
        console.log 'Request JS trying coffee'
        script = script.replace /\.[^.]+$/, '.coffee'
        fs.stat "#{cliDir}/#{script}", (err, stats)->
          if err
            console.log 'Request JS cant find', script
            res.writeHead 404, 'Content-Type': 'text/plain'
            res.end "Cant find #{script}"
          else
            res.writeHead 200, 'Content-Type': 'application/javascript'
            res.end getOutput 'coffee', '-p', "#{cliDir}/#{script}"
      else
        res.writeHead 200, 'Content-Type': 'application/javascript'
        res.end loadFileContent script

  else
    console.log 'ignored request:', decodeURI req.url
    res.end 'ignored request.'

require('./x11action').init (err, action, x11, display)->
  w = h = null

  action.getScreenDimnsion (err, geom)->
    w = geom.width
    h = geom.height

  io = require('socket.io') srv
  io.on 'connection', (socket)->
    console.log '>> Client Connected.'

    socket.on 'disconnect', -> console.log '>> Disconnected.'

    socket.on 'debug', (data)-> console.log '>> debug', data

    socket.on 'mouse-move', ({x,y})-> action.mouseMove round(x*w), round(y*h)

    socket.on 'mouse-click', (bt)-> action.mouseClick bt

    socket.on 'input-text', (txt)-> action.inputText txt

    socket.on 'key-press', (key)-> action.keyPress key

    socket.on 'run-app', (codeName)->
      app = apps.list[codeName]
      console.log 'Run App', codeName
      cmd = app.cmd.replace /%[A-Z]+/, ''
      #TODO: replace zenity by node-x11 based code
      spawn 'zenity', ['--info', '--timeout=4', "--title=Launching App",
                       "--text=Loading #{app.name}...\n#{app.description}\n#{cmd}",
                       "--icon-name=#{app.icon}"]
      process.stdout.write '\x07'
      spawn 'sh', ['-c', cmd]

    socket.emit 'updateApps', apps.list

  myIF = 'iface'
  myIP = '?.?.?.?'
  os = require 'os'
  ifaces = do os.networkInterfaces
  for ifname,iface of ifaces
    for addr in iface
      if addr.family is 'IPv4' and not addr.internal
        myIF = ifname
        myIP = addr.address
  srv.listen 2345, -> console.log "Server is On at #{myIF} http://#{myIP}:2345."

