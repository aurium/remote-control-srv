exports.run = run = require('child_process').spawnSync

exports.getOutput = (bin, args...)->
  out = run bin, args
  if out.status is 0
    out.stdout.toString()
  else
    console.error "getOutput from #{bin} fail: #{out.stderr.toString()}"

