fs = require 'fs'
ini = require 'ini'
{spawnSync} = require 'child_process'

validEntry = (entry)->
  entry.Type is 'Application' and entry.Categories
  #and not /^X-/.test(entry.Categories)

module.exports.loadAppList = ->
  module.exports.list = apps = {}
  [
    ['sys', '/usr/share/applications'],
    ['usr', "#{process.env.HOME}/.local/share/applications"]
  ].map ([place, colectionDir])->
    console.log "Search Linux Apps at \"#{colectionDir}\"."
    fs.readdirSync(colectionDir).map (desktopFile)->
      if /\.desktop$/.test desktopFile
        desktopFilePath = colectionDir + '/' + desktopFile
        try
          model = ini.parse fs.readFileSync desktopFilePath, 'utf-8'
          entry = model['Desktop Entry']
          if validEntry entry
            appInfo = {}
            appInfo.codeName = "#{place}:#{desktopFile.replace /.desktop$/, ''}"
            appInfo.name = entry.Name || appInfo.codeName
            appInfo.description = entry.Comment
            appInfo.categories = entry.Categories.replace(/[; ]+$/,'').split ';'
            appInfo.icon = entry.Icon
            #appInfo.mimeType = entry.MimeType
            #appInfo.desktopEntry = desktopFilePath
            appInfo.cmd = entry.Exec
            apps[appInfo.codeName] = appInfo
            #console.log appInfo.name, appInfo.categories
        catch err
          console.error "Cant load #{desktopFilePath}", err
  console.log "#{Object.keys(apps).length} apps loaded."
  apps

iconCache = {}
module.exports.findIcon = (baseName)->
  return iconCache[baseName] if iconCache[baseName]
  iconCache[baseName] = if baseName[0] is '/'
    baseName
  else
    spawnSync('bash', ['-c', @findIconSH baseName])
             .stdout.toString().replace /\s+$/, ''
  iconCache[baseName]

module.exports.findIconSH = (baseName)->
  # Should match image type: scalable, 32x32, 48x48, 64x64, 96x96.
  # (crazy between sizes, like 72x72, are ok)
  # Should match asset type apps, apps-extra, and other apps*.
  baseName = baseName.replace /\....$/, ''
  iconsDir = "/usr/share #{process.env.HOME}/.local"
  dirRE = '(/icons/|/pixmaps/|/scalable/apps|/[3-9][0-9]x[3-9][0-9]/apps)'
  ext = '(svg|png|jpg|ico)'
  """
    list="$(
      /usr/bin/find #{iconsDir} -regextype posix-egrep \
                                -iregex '.*#{dirRE}.*/#{baseName}\.#{ext}'
    )"
    # try to filter by favorited themes
    list="$(if !( echo "$list" | egrep -i '/(tango|gnome|kde|xfce)/' ); then
      echo "$list" # if grep fails, throw full list
    fi)"
    # prefer SVG icon
    list="$(if !( echo "$list" | grep 'svg$' ); then
      echo "$list" # if grep fails, throw full list
    fi)"
    echo "$list" | tail -n1
  """

