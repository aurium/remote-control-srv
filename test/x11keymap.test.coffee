should = require 'should'
inspect = require('util').inspect

describe 'X11 keymap', ->

  keymap = null

  before (done)->
    require('../x11keymap').init (err, __keymap, x11, display)->
      keymap = __keymap
      do done

  it 'Sync load', ->
    delete require.cache[module] for module of require.cache
    keymap = do require('../x11keymap').init
    keymap.table.constructor.should.be.equal Array

  it 'Async load', (done)->
    delete require.cache[module] for module of require.cache
    require('../x11keymap').init (err, __keymap, x11, display)->
      __keymap.table.constructor.should.be.equal Array
      do done

  it 'keyCode Table', ->
    keymap.table[24].should.be.deepEqual ['q', 'Q', 'q', 'Q', 'slash', 'slash']

  it 'findKey', ->
    keymap.findKey('ISO_level3_shift').keycode.should.be.equal 92
    keymap.findKey('ISO_level3_shift').name.should.be.equal 'iso_level3_shift'
    keymap.findKey('iso_level3_shift').keycode.should.be.equal 92
    keymap.findKey('iso_level3_shift').name.should.be.equal 'iso_level3_shift'
    key = keymap.findKey('a')
    key.keycode.should.be.equal 38
    key.name.should.be.equal 'a'
    key.keycodeList.should.be.deepEqual [38]
    key = keymap.findKey('A')
    key.keycode.should.be.equal 38
    key.name.should.be.equal 'A'
    key.keycodeList.should.be.deepEqual [50, 38]
    key = keymap.findKey('?')
    key.keycode.should.be.equal 25
    key.name.should.be.equal 'question'
    key.keycodeList.should.be.deepEqual [92, 25]
    key = keymap.findKey('/')
    key.keycode.should.be.equal 24
    key.name.should.be.equal 'slash'
    key.keycodeList.should.be.deepEqual [92, 24]
    key = keymap.findKey('€')
    key.keycode.should.be.equal 26
    key.name.should.be.equal 'eurosign'
    key.keycodeList.should.be.deepEqual [92, 26]

